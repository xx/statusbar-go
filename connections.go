package main

import (
	"context"

	"github.com/godbus/dbus/v5"
	sway "github.com/joshuarubin/go-sway"
	"github.com/noisetorch/pulseaudio"
)

var CONN Connections

type Connections struct {
	dbusSession, dbusSystem *dbus.Conn
	paClient                *pulseaudio.Client
	swayClient              sway.Client
}

func init_connections() (Connections, error) {
	ctx := context.Background()

	dbusSession, err := dbus.ConnectSessionBus()
	if err != nil {
		return Connections{}, err
	}

	dbusSystem, err := dbus.ConnectSystemBus()
	if err != nil {
		return Connections{}, err
	}

	paClient, err := pulseaudio.NewClient()
	if err != nil {
		return Connections{}, err
	}

	swayClient, err := sway.New(ctx)
	if err != nil {
		return Connections{}, err
	}

	return Connections{
		dbusSession: dbusSession,
		dbusSystem:  dbusSystem,
		paClient:    paClient,
		swayClient:  swayClient,
	}, nil
}

func (c Connections) close_all() {
	c.dbusSession.Close()
	c.dbusSystem.Close()
	c.paClient.Close()
}
