package main

import "math"

func pulseaudio_handler(feedback chan bool) {
	vol, muted := get_default_sink_volume()

	sigs, err := CONN.paClient.Updates()
	if err != nil {
		warn("Failed to create PulseAudio update listener!", err)
		return
	}

	for range sigs {
		nVol, nMuted := get_default_sink_volume()
		if nVol != vol || nMuted != muted {
			feedback <- true
			vol = nVol
			muted = nMuted
		}
	}
}

func get_default_sink_volume() (int, bool) {
	si, err := CONN.paClient.ServerInfo()
	if err != nil {
		return 0, false
	}

	sinks, err := CONN.paClient.Sinks()
	if err != nil {
		return 0, false
	}

	for i := range sinks {
		if sinks[i].Name == si.DefaultSink {
			return int(math.Round(float64(sinks[i].Cvolume[0]) / float64(sinks[i].BaseVolume) * 100)), sinks[i].Muted
		}
	}

	return 0, false
}
