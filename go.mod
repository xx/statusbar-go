module git.dog/xx/statusbar-go

go 1.19

require github.com/godbus/dbus/v5 v5.1.0

require (
	github.com/joshuarubin/go-sway v1.2.0
	github.com/noisetorch/pulseaudio v0.0.0-20220603053345-9303200c3861
)

require (
	github.com/joshuarubin/lifecycle v1.1.4 // indirect
	go.uber.org/multierr v1.11.0 // indirect
	golang.org/x/sync v0.8.0 // indirect
)
