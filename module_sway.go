package main

import (
	"context"

	sway "github.com/joshuarubin/go-sway"
)

type sway_handler struct {
	sway.EventHandler
	feedback chan bool
}

func (h sway_handler) Input(ctx context.Context, ev sway.InputEvent) {
	if ev.Change != "xkb_layout" || ev.Input.Type != "keyboard" {
		return
	}

	oldval, _ := CACHE.Load(CACHE_LAYOUT)
	if oldval == *ev.Input.XKBActiveLayoutName {
		return
	}

	CACHE.Store(CACHE_LAYOUT, *ev.Input.XKBActiveLayoutName)
	h.feedback <- true
}

func sway_input_handler(feedback chan bool) {
	ctx := context.Background()

	var h sway.EventHandler = sway_handler{feedback: feedback}

	sway.Subscribe(ctx, h, sway.EventTypeInput)
}

func get_current_layout() string {
	ctx := context.Background()
	inputs, err := CONN.swayClient.GetInputs(ctx)
	if err != nil {
		warn("Failed to get Sway inputs!", err)
	}

	for _, input := range inputs {
		if input.Type != "keyboard" {
			continue
		}

		return *input.XKBActiveLayoutName
	}

	return ""
}

func long_layout_to_short(layout string) string {
	switch layout {
	case "English (US)":
		return "US"
	case "Estonian":
		return "ET"
	}

	return layout
}
