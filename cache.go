package main

import (
	"sync"
)

const (
	// cache key definitions
	CACHE_BT_CONNECTED = "bt_connected"
	CACHE_BT_BATTERY   = "bt_battery"
	CACHE_MEDIA        = "media"
	CACHE_LAYOUT       = "kbd_layout"
	CACHE_TOGGL        = "toggl_project"
	CACHE_BATTERY      = "battery_status"
)

var CACHE = sync.Map{}

func getC[T comparable](c *sync.Map, id string, fallback func() T) T {
	val, ok := c.Load(id)
	if !ok {
		val = fallback()
		c.Store(id, val)
	}

	return val.(T)
}
